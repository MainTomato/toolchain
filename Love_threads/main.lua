local path = (arg[0] or arg[1]):gsub('\\', '/'):match('(.*/)')
package.path = path..'?.lua;'..package.path

Thread = require'thread'

-- [thread] = Thread('name', 'string or path to file' [, callback list])

a = Thread('A_thread', [[

	-- link to other thread
	local B_link = thread:newLink('B_thread')
	
	-- thread:start(...) aruments passed to table arg
	B_link:send(arg[1])
	
	while true do
	
		-- receive data from main thread
		local data = thread:receive()
		if data then
			-- send data to linked thread
			B_link:send(data)
		end
	end
	
]])


-- List of callbacks:
--- receive: function(self, msg) Calls if thread send the message to parent program
--- finish:  function(self, ...) Calls if thread finish self work. '...' contain all unreaded messages
--- error:   function(self, err) Calls if thread got error
--- kill:    function(self)      Calls if parent program kill the thread

local b_callback = {}

-- 'self' is attached thread
function b_callback:receive(data)
	a:send(data) 
end

b = Thread('B_thread', [[

	-- link to other thread
	local A_link = thread:newLink('A_thread')	
	
	while true do
		local data = A_link:receive()
		if data then
			print(data)
			thread:send(data)
		end
	end
]], b_callback)


a:start('Yo')
b:start()


function love.update()
	-- calling callbacks etc
	a:update()
	b:update()
	
end
