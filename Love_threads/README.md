### Thread library for LOVE2d ###
v0.8

Compatible with love 0.9.n - 0.10.n

This library provide thread-class with callbacks and some stuff!

#Usage:#
##Main thread##
```
#!lua
Thread = require'thread'

-- Create new threads
thread = Thread(name, code[, callbacks])
-- name (string) need for named channels
-- code (string) is chunk of code or path to file
-- callbacks is a table with functions, keys is:
--- receive: function(self, msg) Calls if thread send the message to parent program
--- finish:  function(self, tbl) Calls if thread finish self work. tbl contain all unreaded messages
--- error:   function(self, err) Calls if thread got error
--- kill:    function(self)      Calls if parent program kill the thread and garbage is collected

thread:setCallback(name, func)
-- set specified callback

thread:start( ... )
-- ... is arguments, can be lua or love data, flat tables etc,
-- passed inside thread to 'arg'-table

thread:send(data)
-- data (number/string/flat-table/love-data)

data = thread:receive()
-- data (number/string/flat-table/love-data)

t = thread:receiveAll()
-- t is list of all unreaded messages from threads

thread:clear()
-- clear send and receive pools

out, in = thread:getCount()
-- get count of out and in message-pools



thread:kill()
-- instantly kill the thread

thread:update()
-- call callbacks, print errors to default stdin

thread:wait()

thread:isRunning()
-- Get thread running-state

thread:getError()
-- if you don't want to update threads

thread:supply(data)
-- watch on http://love2d.org/wiki/Channel

```

##Child threads##
```
#!lua

thread:send(data)
-- send data to parent thread

data = thread:receive()
-- receive data from parent thread

tbl = thread:receive()
-- receive all messages from parent thread


link = thread:newLink(name)
-- name (string) of other thread, we want to link

link:send(data)
-- send data to link

data = link:receive(data)
-- reeive data from link

tbl = link:receiveAll(data)
-- reeive all data from link

link:clean()
-- clear send and receive pools

out, in = link:getCount()
-- get count of out and in message-pools

```