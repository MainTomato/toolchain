### Luna as Lua UDP Network Adapter v0.2###

This library provide channel manager with callback system and async non-blocking receive-loop.

#Usage:#
```
#!lua
-- Master
-- Initialisation
luna = require'luna'([port])

-- Listening to the port, wait income data
luna:listen('port')

-- Adding new callbacks
-- Message here is any serialisable data
luna:setCallback('callback name', function(connection, Message))

-- Service callbacks, called on inner events:
-- 'connect' as connect event, useful for login connections and filter/firewall
-- 'disconnect' as disconnect event
-- 'ping' on ping request
-- 'pong' on ping response


-- Income messages iteration, also it makes all magic with non-blocking loops
for data, ip, port in luna:receive() do
 -- Here are raw (not decoded) messages for which there has no collback
end

-- Close, send 'disconnect' to all connections and close it
luna:close([Message])

-- Debug log, using default print output
luna:debug(bool)


-- Connection
-- Initialisation
connection = luna:connect('ip/host', 'port'[, Message])

-- Sending
connection:send(Message, 'callback name')

-- Closing
connection:close([, Message])

-- Status
status = connection:status() -- return 'unconnected' or 'connected', if other side response to pings


-- More complex example on tests
```


###Limits:###
* Luasocket can transfer only 8k-messages. It can be stronger.
* Data transfer has same limits as json library: you can't transfer classes or tables with functions/userdata, it needs to be serialised outside Luna.
* Async logic can be too difficult for novices, we need simpler ways (receive function can be useful here)
* Not so fast as possible. Can be improuved with cjson module.

###Issues###
* Reliable UDP protocol between Luna and luasocket for sending any lenth messages (in development)
* Any-data serialiser with classes and cdata (with callbacks for (de)serialisation?)
* Autoswitch between ipv4/ipv6