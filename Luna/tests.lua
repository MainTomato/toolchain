local path = (arg[0] or arg[1]):gsub('\\', '/'):match('(.*/)')
package.path = path..'?.lua;'..package.path

Luna = require'Luna.init' -- or 'Luna'

luna = Luna()

luna:listen(3470)

Localhost = luna:connect('localhost', 3470)

luna:setCallback(
	'Hi!', 
	function(connection, data)
		print(data)
		connection:send('Hello you too!', 'Hello!')
	end
)

luna:setCallback(
	'Hello!', 
	function(connection, data)
		print(data)
		connection:send('Hello hello hello!', 'Hi!')
	end
)

Localhost:send({field = 'I say you hi!'}, 'Hi!')

while 1 do
	for data, ip, port in luna:receive() do
		print('received', data, ip, port)
	end
end
